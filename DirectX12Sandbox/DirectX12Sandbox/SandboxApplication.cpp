#include "SandboxApplication.h"
#include "Constants.h"
#include <d2d1.h>

//enum classes > enum disable
#pragma warning(disable:26812)

float SandboxApplication::PlotLeft(int column)
{
	return (float)(column * Constants::CELL_WIDTH);
}

float SandboxApplication::PlotRight(int column)
{
	return PlotLeft(column) + (float)Constants::CELL_WIDTH;
}

float SandboxApplication::PlotTop(int row)
{
	return (float)(row * Constants::CELL_HEIGHT);
}

float SandboxApplication::PlotBottom(int row)
{
	return PlotTop(row) + (float)Constants::CELL_HEIGHT;
}

D2D1_RECT_F SandboxApplication::PlotCell(int column, int row)
{
	return D2D1::RectF(
		PlotLeft(column),
		PlotTop(row),
		PlotRight(column),
		PlotBottom(row));
}

void SandboxApplication::InitializeScoreDigitDestinations()
{
	for (int index = 0; index < Constants::SCORE_DIGIT_COUNT; ++index)
	{
		//make positions go from right to left
		int position = Constants::SCORE_DIGIT_COUNT - 1 - index;
		//positions are offset one cell to the right(+1 to column)
		scoreDigitDestinations[position] = PlotCell(index + 1, 0);
	}
}

void SandboxApplication::InitializeScoreDigitSources()
{
	for (int index = 0; index < Constants::SCORE_RADIX; ++index)
	{
		scoreDigitSources[index] = PlotCell(index, 0);
	}
}

SandboxApplication::SandboxApplication()
	: factory(nullptr)
	, renderTarget(nullptr)
	, blocks()
	, tail()
	, brushes()
	, counter(0.0)
	, direction(1)
	, gameOver(true)
	, numbersBitmap(nullptr)
	, scoreDigitDestinations()
	, scoreDigitSources()
	, score(0)
	, runLength(0)
{
	InitializeScoreDigitDestinations();
	InitializeScoreDigitSources();
	ResetGame();
}

static D2D1_COLOR_F brushColors[SANDBOXBRUSH_COUNT] =
{
	D2D1::ColorF(1.0f, 1.0f, 1.0f, 1.0f), //white - block
	D2D1::ColorF(0.0f, 0.0f, 1.0f, 1.0f), //blue - wall
	D2D1::ColorF(1.0f, 1.0f, 0.0f, 1.0f), //yellow - tail
	D2D1::ColorF(1.0f, 0.0f, 0.0f, 1.0f)  //red - head
};

BOOL SandboxApplication::OnCreateWindow()
{
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &factory);

	D2D1_SIZE_U size = D2D1::SizeU(Constants::SCREEN_WIDTH, Constants::SCREEN_HEIGHT);
	factory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(GetWindow(), size), &renderTarget);

	for (int index = 0; index < SANDBOXBRUSH_COUNT; ++index)
	{
		renderTarget->CreateSolidColorBrush(brushColors[index], &(brushes[index]));
	}

	IWICImagingFactory* imagingFactory = nullptr;
	if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, __uuidof(IWICImagingFactory), (LPVOID*)&imagingFactory)))
	{

	}
	IWICBitmapDecoder* decoder = nullptr;
	imagingFactory->CreateDecoderFromFilename(Constants::NUMBERS_BITMAP_FILENAME, nullptr, GENERIC_READ, WICDecodeMetadataCacheOnLoad, &decoder);
	IWICBitmapFrameDecode* source = nullptr;
	decoder->GetFrame(0, &source);
	IWICFormatConverter* convertor;
	imagingFactory->CreateFormatConverter(&convertor);
	convertor->Initialize(source, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, nullptr, 0.0f, WICBitmapPaletteTypeMedianCut);
	renderTarget->CreateBitmapFromWicBitmap(convertor, nullptr, &numbersBitmap);
	SafeRelease(convertor);
	SafeRelease(source);
	SafeRelease(decoder);
	SafeRelease(imagingFactory);

	return TRUE;
}

BOOL SandboxApplication::OnDestroyWindow()
{
	PostQuitMessage(0);
	return TRUE;
}

void SandboxApplication::DrawBlocks()
{
	for (int index = 0; index < Constants::BOARD_ROWS; ++index)
	{
		float x = (float)(blocks[index] * Constants::CELL_WIDTH);
		float y = (float)(index * Constants::CELL_HEIGHT);
		renderTarget->FillRectangle(D2D1::RectF(x, y, x + Constants::CELL_WIDTH, y + Constants::CELL_HEIGHT), brushes[SANDBOXBRUSH_BLOCK]);
	}
}

void SandboxApplication::DrawTail()
{
	for (int index = 0; index < Constants::TAIL_LENGTH; ++index)
	{
		float x = (float)(tail[index] * Constants::CELL_WIDTH);
		float y = (float)(index * Constants::CELL_HEIGHT);
		renderTarget->FillRectangle(D2D1::RectF(x, y, x + Constants::CELL_WIDTH, y + Constants::CELL_HEIGHT), brushes[(index < Constants::TAIL_LENGTH - 1) ? (SANDBOXBRUSH_TAIL) : (SANDBOXBRUSH_HEAD)]);
	}
}

void SandboxApplication::DrawWalls()
{
	renderTarget->FillRectangle(D2D1::RectF(0, 0, Constants::CELL_WIDTH, Constants::SCREEN_HEIGHT), brushes[SANDBOXBRUSH_WALL]);
	renderTarget->FillRectangle(D2D1::RectF(Constants::SCREEN_WIDTH - Constants::CELL_WIDTH, 0, Constants::SCREEN_WIDTH, Constants::SCREEN_HEIGHT), brushes[SANDBOXBRUSH_WALL]);
}

void SandboxApplication::DrawScore()
{
	int temp = score;
	for (int index = 0; index < Constants::SCORE_DIGIT_COUNT; ++index)
	{
		renderTarget->DrawBitmap(numbersBitmap, scoreDigitDestinations[index], 1.0f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, scoreDigitSources[temp % Constants::SCORE_RADIX]);
		temp /= Constants::SCORE_RADIX;
	}
}

void SandboxApplication::Draw()
{
	renderTarget->BeginDraw();
	renderTarget->Clear(D2D1::ColorF(0.0f, 0.0f, 0.0f));
	DrawTail();
	DrawBlocks();
	DrawWalls();
	DrawScore();
	renderTarget->EndDraw();
}

void SandboxApplication::ScrollBookkeeping()
{
	runLength++;
	counter -= Constants::SCROLL_COUNTER;
}

void SandboxApplication::ScrollBlocks()
{
	for (int index = 0; index < Constants::BOARD_ROWS - 1; ++index)
	{
		blocks[index] = blocks[index + 1];
	}
	blocks[Constants::BOARD_ROWS - 1] = rand() % (Constants::BOARD_COLUMNS - 2) + 1;
}

void SandboxApplication::ScrollTail()
{
	for (int index = 0; index < Constants::TAIL_LENGTH - 1; ++index)
	{
		tail[index] = tail[index + 1];
	}
	tail[Constants::TAIL_LENGTH - 1] = tail[Constants::TAIL_LENGTH - 1] + direction;
}

void SandboxApplication::CheckForGameOver()
{
	gameOver =
		tail[Constants::TAIL_LENGTH - 1] == blocks[Constants::TAIL_LENGTH - 1] ||
		tail[Constants::TAIL_LENGTH - 1] == Constants::BOARD_COLUMNS - 1 ||
		tail[Constants::TAIL_LENGTH - 1] == 0;
}

void SandboxApplication::ScrollBoard()
{
	ScrollBookkeeping();
	ScrollBlocks();
	ScrollTail();
	CheckForGameOver();
}

void SandboxApplication::Update(double delta)
{
	if (!gameOver)
	{
		counter = counter + delta;
		while (!gameOver && counter > Constants::SCROLL_COUNTER)
		{
			ScrollBoard();
		}
	}
}

void SandboxApplication::AwardScore()
{
	score += runLength * (runLength + 1) / 2;
	runLength = 0;
}

BOOL SandboxApplication::OnKeyDown(WPARAM wParam)
{
	if (!gameOver)
	{
		if (wParam == VK_LEFT && direction == 1)
		{
			AwardScore();
			direction = -1;
		}
		else if (wParam == VK_RIGHT && direction == -1)
		{
			AwardScore();
			direction = 1;
		}
	}
	else
	{
		if (wParam == VK_SPACE)
		{
			ResetGame();
			gameOver = false;
		}
	}
	return TRUE;
}

BOOL SandboxApplication::HandleMessage(UINT message, WPARAM wParam, LPARAM)
{
	switch (message)
	{
	case WM_KEYDOWN:
		return OnKeyDown(wParam);
	case WM_CREATE:
		return OnCreateWindow();
	case WM_DESTROY:
		return OnDestroyWindow();
	default:
		break;
	}
	return FALSE;
}
Application* SandboxApplication::Create()
{
	return new SandboxApplication();
}

SandboxApplication::~SandboxApplication()
{
	SafeRelease(numbersBitmap);
	for (int index = 0; index < SANDBOXBRUSH_COUNT; ++index)
	{
		SafeRelease(brushes[index]);
	}
	SafeRelease(renderTarget);
	SafeRelease(factory);
}

void SandboxApplication::ResetGame()
{
	for (int index = 0; index < Constants::BOARD_ROWS; ++index)
	{
		blocks[index] = 0;
	}
	for (int index = 0; index < Constants::TAIL_LENGTH; ++index)
	{
		tail[index] = Constants::BOARD_COLUMNS / 2;
	}
	direction = 1;
	score = 0;
	runLength = 0;
}
