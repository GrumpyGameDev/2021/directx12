#pragma once
#include "Application.h"
#include "Constants.h"
#include <d2d1.h>
#include <wincodec.h>
enum
{
	SANDBOXBRUSH_BLOCK,
	SANDBOXBRUSH_WALL,
	SANDBOXBRUSH_TAIL,
	SANDBOXBRUSH_HEAD,
	SANDBOXBRUSH_COUNT
};
class SandboxApplication: public Application
{
private:
	ID2D1Factory* factory;
	ID2D1HwndRenderTarget* renderTarget;

	ID2D1SolidColorBrush* brushes[SANDBOXBRUSH_COUNT];

	ID2D1Bitmap* numbersBitmap;
	D2D1_RECT_F scoreDigitDestinations[Constants::SCORE_DIGIT_COUNT];
	D2D1_RECT_F scoreDigitSources[Constants::SCORE_RADIX];

	int blocks[Constants::BOARD_ROWS];
	int tail[Constants::TAIL_LENGTH];
	int direction;
	bool gameOver;
	int score;
	int runLength;
	double counter;

	SandboxApplication();
	BOOL OnCreateWindow();
	BOOL OnDestroyWindow();
	BOOL OnKeyDown(WPARAM);
	void ResetGame();
	void AwardScore();
	void DrawBlocks();
	void DrawTail();
	void DrawWalls();
	void DrawScore();
	void ScrollBoard();
	void ScrollBookkeeping();
	void ScrollBlocks();
	void ScrollTail();
	void CheckForGameOver();
	void InitializeScoreDigitDestinations();
	void InitializeScoreDigitSources();
	static float PlotLeft(int);
	static float PlotRight(int);
	static float PlotTop(int);
	static float PlotBottom(int);
	static D2D1_RECT_F PlotCell(int, int);
protected:
	BOOL HandleMessage(UINT, WPARAM, LPARAM);
	void Update(double);
	void Draw();
public:
	static Application* Create();
	~SandboxApplication();
};

