#include "Application.h"
#include "Constants.h"
#include <objbase.h>

Application::Application()
	: window(NULL)
{

}

LRESULT Application::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	Application* application = (Application*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	if (application)
	{
		if (application->HandleMessage(message, wParam, lParam))
		{
			return 0;
		}
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	else
	{
		switch (message)
		{
		case WM_CREATE:
		{
			CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
			Application* application = (Application*)cs->lpCreateParams;
			if (application)
			{
				SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)application);
				application->window = hWnd;
				application->HandleMessage(message, wParam, lParam);
			}
			return 0;
		}
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
}

static ATOM CreateWindowClass(HINSTANCE hInstance, WNDPROC windowProc)
{
	WNDCLASSEXW wcex = { 0 };
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wcex.lpfnWndProc = windowProc;
	wcex.hInstance = hInstance;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.lpszClassName = L"DIRECTX12SANDBOX";
	return RegisterClassExW(&wcex);
}

static bool CreateApplicationWindow(ATOM atom, LPCWSTR szTitle, HINSTANCE hInstance, int nCmdShow, Application* application)
{
	RECT rc;
	SetRect(&rc, 0, 0, (int)Constants::SCREEN_WIDTH, (int)Constants::SCREEN_HEIGHT);
	AdjustWindowRect(&rc, WS_CAPTION | WS_SYSMENU, FALSE);
	HWND hWnd =
		CreateWindowExW
		(
			0L,
			(LPCWSTR)atom,
			szTitle,
			WS_CAPTION | WS_SYSMENU,
			CW_USEDEFAULT,
			0,
			rc.right - rc.left,
			rc.bottom - rc.top,
			nullptr,
			nullptr,
			hInstance,
			application
		);
	if (!hWnd)
	{
		return false;
	}
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	return true;
}

int Application::Run(HINSTANCE hInstance, int nCmdShow, LPCWSTR szTitle, ApplicationFactory factory)
{
	int result = 0;
	if (S_OK == CoInitialize(NULL))
	{
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		LARGE_INTEGER lastCounter;
		QueryPerformanceCounter(&lastCounter);
		LARGE_INTEGER counter;
		ATOM atom = CreateWindowClass(hInstance, Application::WndProc);
		Application* application = (factory) ? (factory()) : nullptr;
		if (CreateApplicationWindow(atom, szTitle, hInstance, nCmdShow, application))
		{
			MSG msg;
			bool done = false;

			while (!done)
			{
				if (PeekMessage(&msg, nullptr, 0, 0, 1))
				{
					done = msg.message == WM_QUIT;
					if (!done)
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
				}
				else
				{
					QueryPerformanceCounter(&counter);
					double delta = (double)(counter.QuadPart - lastCounter.QuadPart) / (double)frequency.QuadPart;
					lastCounter = counter;
					application->Update(delta);
					application->Draw();
				}
			}
			result = (int)msg.wParam;
		}
		if (application)
		{
			delete application;
		}
		CoUninitialize();
	}
	return result;
}
