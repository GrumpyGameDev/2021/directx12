#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
class Application;
typedef Application* (*ApplicationFactory)();

class Application
{
private:
	HWND window;
	static LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);
protected:
	template <class T> static void SafeRelease(T*& ptr) { if (ptr) { ptr->Release(); ptr = nullptr; } }
	HWND GetWindow() { return window; }
	virtual BOOL HandleMessage(UINT, WPARAM, LPARAM) = 0;
	virtual void Update(double) = 0;
	virtual void Draw() = 0;
	Application();
public:
	static int Run(HINSTANCE, int, LPCWSTR, ApplicationFactory);
	virtual ~Application() {}
};

