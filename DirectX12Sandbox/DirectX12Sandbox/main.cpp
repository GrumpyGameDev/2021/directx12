#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "SandboxApplication.h"


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	return Application::Run(hInstance, nCmdShow, L"JetLag 2021", SandboxApplication::Create);
}

