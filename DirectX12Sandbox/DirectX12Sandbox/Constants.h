#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
class Constants
{
public:
	static const int BOARD_COLUMNS = 40;
	static const int BOARD_ROWS = 30;
	static const int CELL_WIDTH = 16;
	static const int CELL_HEIGHT = 16;
	static const unsigned int SCREEN_WIDTH = BOARD_COLUMNS * CELL_WIDTH;
	static const unsigned int SCREEN_HEIGHT = BOARD_ROWS * CELL_HEIGHT;
	static const double SCROLL_COUNTER;
	static const int TAIL_LENGTH = 6;
	static const int SCORE_DIGIT_COUNT = 8;
	static const int SCORE_RADIX = 10;
	static const LPCWSTR NUMBERS_BITMAP_FILENAME;
};

